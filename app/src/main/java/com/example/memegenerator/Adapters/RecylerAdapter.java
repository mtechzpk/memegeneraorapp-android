package com.example.memegenerator.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.memegenerator.Models.RecyclerModel;
import com.example.memegenerator.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecylerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<RecyclerModel> recyclerModels;

    public RecylerAdapter(Context context, ArrayList<RecyclerModel> recyclerModels) {
        this.context = context;
        this.recyclerModels = recyclerModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_grid_item, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;

        holder1.bind(position);

    }

    @Override
    public int getItemCount() {
        return recyclerModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvUsernaem;
        CircleImageView country_flag;

        private BookViewHolder(View itemView) {
            super(itemView);

            //init views
            tvUsernaem    = itemView.findViewById(R.id.name);
//            country_flag      = itemView.findViewById(R.id.country_flag);
        }

        private void bind(int pos) {
            RecyclerModel messagesTabModel = recyclerModels.get(pos);
            tvUsernaem.setText(messagesTabModel.getName());
//            initClickListener();
        }
    }
}
