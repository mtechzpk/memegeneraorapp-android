package com.example.memegenerator.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.memegenerator.Activities.CustomMemeActivity;
import com.example.memegenerator.Activities.MainActivity;
import com.example.memegenerator.Models.Item;
import com.example.memegenerator.R;

import java.util.ArrayList;
import java.util.List;

public class NewTabAdapter extends RecyclerView.Adapter<NewTabAdapter.ItemViewHolder> {
    public static final int SPAN_COUNT_ONE = 1;
    public static final int SPAN_COUNT_THREE = 3;
private Context context;
    private static final int VIEW_TYPE_SMALL = 1;
    private static final int VIEW_TYPE_BIG = 2;

    private List<Item> mItems;
    private GridLayoutManager mLayoutManager;

    public NewTabAdapter(Context context, List<Item> items, GridLayoutManager layoutManager) {
        mItems = items;
        this.context = context;
        mLayoutManager = layoutManager;
    }
    @Override
    public int getItemViewType(int position) {
        int spanCount = mLayoutManager.getSpanCount();
        if (spanCount == SPAN_COUNT_ONE) {
            return VIEW_TYPE_BIG;
        } else {
            return VIEW_TYPE_SMALL;
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == VIEW_TYPE_BIG) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_big, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_small, parent, false);
        }
        return new ItemViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        List<Item> items = new ArrayList<>();
        Item item = mItems.get(position % 4);
        holder.title.setText(item.getTitle());
        holder.iv.setImageResource(item.getImgResId());
        holder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, CustomMemeActivity.class);
                context.startActivity(intent);
            }
        });
//        if (getItemViewType(position) == VIEW_TYPE_BIG) {
//            holder.info.setText(item.getLikes() + " likes  ·  " + item.getComments() + " comments");
//        }
    }

    @Override
    public int getItemCount() {
        return 30;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        ImageView iv;
        TextView title;
        TextView info;
        LinearLayout llItem;

        ItemViewHolder(View itemView, int viewType) {
            super(itemView);
            if (viewType == VIEW_TYPE_BIG) {
                iv = (ImageView) itemView.findViewById(R.id.image_big);
                title = (TextView) itemView.findViewById(R.id.title_big);
//                info = (TextView) itemView.findViewById(R.id.tv_info);
                llItem = itemView.findViewById(R.id.llItem);
            } else {
                iv = (ImageView) itemView.findViewById(R.id.image_small);
                title = (TextView) itemView.findViewById(R.id.title_small);
                llItem = itemView.findViewById(R.id.llItem);
            }
        }
    }
}
