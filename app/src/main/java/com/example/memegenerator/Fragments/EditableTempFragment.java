package com.example.memegenerator.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.memegenerator.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditableTempFragment extends Fragment {
TextView tvTitle;
View view;
    public EditableTempFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_editable_temp, container, false);
        return view;
    }
}
