package com.example.memegenerator.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.memegenerator.Adapters.PopularTabAdapter;
import com.example.memegenerator.Adapters.RecylerAdapter;
import com.example.memegenerator.Models.PopularTabModel;
import com.example.memegenerator.Models.RecyclerModel;
import com.example.memegenerator.R;

import java.util.ArrayList;

public class FavoriteTab extends Fragment {
    public FavoriteTab() {
        // Required empty public constructor
    }

    private View v;
    private RecyclerView rv_fav;
    private PopularTabAdapter pAdapter;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<PopularTabModel> popularTabModels;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_favorite_tab, container, false);
        init();
        return v;
    }

    private void init() {
        rv_fav = v.findViewById(R.id.rv_fav);

        initAdapter();
    }

    private void initAdapter() {
        setEventsName();
        pAdapter = new PopularTabAdapter(getContext(), popularTabModels);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rv_fav.setLayoutManager(mLayoutManager);
        rv_fav.setAdapter(pAdapter);
        rv_fav.setHasFixedSize(true);
    }

    private void setEventsName() {
        popularTabModels = new ArrayList<>();
        for (int i = 1; i <= 13; i++) {
            PopularTabModel recyclerModel = new PopularTabModel();
            recyclerModel.setName("Image " + i);
            popularTabModels.add(recyclerModel);
        }
    }
}
