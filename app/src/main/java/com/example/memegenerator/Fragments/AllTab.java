package com.example.memegenerator.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.memegenerator.Adapters.ItemAdapter;
import com.example.memegenerator.Adapters.RecylerAdapter;
import com.example.memegenerator.Adapters.SimpleAdapter;
import com.example.memegenerator.Models.Item;
import com.example.memegenerator.Models.RecyclerModel;
import com.example.memegenerator.R;

import java.util.ArrayList;
import java.util.List;

import static com.example.memegenerator.Adapters.ItemAdapter.SPAN_COUNT_ONE;
import static com.example.memegenerator.Adapters.ItemAdapter.SPAN_COUNT_THREE;

public class AllTab extends Fragment implements View.OnClickListener {
    private ItemAdapter itemAdapter;
    private GridLayoutManager gridLayoutManager;
    private List<Item> items;

    public AllTab() {
        // Required empty public constructor
    }

    private View v;
    private RecyclerView rv_all;
    private RecylerAdapter pAdapter;
    private GridLayoutManager mLayoutManager;
    private ArrayList<RecyclerModel> recyclerModels;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_all_tab, container, false);
        HomeFragment.ivGrid.setOnClickListener(this);
        HomeFragment.ivList.setOnClickListener(this);
        HomeFragment.ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "hello", Toast.LENGTH_SHORT).show();
            }
        });
        init();
        initItemsData();
        gridLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT_THREE);
        itemAdapter = new ItemAdapter(getActivity(),items, gridLayoutManager);
        rv_all.setAdapter(itemAdapter);
        rv_all.setLayoutManager(gridLayoutManager);
        return v;
    }
    private void initItemsData() {
        items = new ArrayList<>(4);
        items.add(new Item(R.drawable.demo_haritik, "Image 1"));
        items.add(new Item(R.drawable.demo_haritik, "Image 2"));
        items.add(new Item(R.drawable.demo_haritik, "Image 3"));
        items.add(new Item(R.drawable.demo_haritik, "Image 4"));
    }
    private void init() {
        rv_all = v.findViewById(R.id.rv_all);

//        initAdapter();


//        pAdapter = new RecylerAdapter(getContext(), recyclerModels);
//        mLayoutManager = new LinearLayoutManager(getActivity());
//        rv_all.setLayoutManager(mLayoutManager);
//        rv_all.setAdapter(pAdapter);

    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.ivGrid:
                HomeFragment.ivList.setVisibility(View.VISIBLE);
                HomeFragment.ivGrid.setVisibility(View.GONE);
                if (gridLayoutManager.getSpanCount() == SPAN_COUNT_ONE) {
                    gridLayoutManager.setSpanCount(SPAN_COUNT_THREE);
                } else {
                    gridLayoutManager.setSpanCount(SPAN_COUNT_ONE);
                }
                break;
            case R.id.ivList:
                if (gridLayoutManager.getSpanCount() == SPAN_COUNT_THREE) {
                    gridLayoutManager.setSpanCount(SPAN_COUNT_ONE);
                } else {
                    gridLayoutManager.setSpanCount(SPAN_COUNT_THREE);
                }
                itemAdapter.notifyItemRangeChanged(0, itemAdapter.getItemCount());
                HomeFragment.ivList.setVisibility(View.GONE);
                HomeFragment.ivGrid.setVisibility(View.VISIBLE);
                break;

        }
    }
}
