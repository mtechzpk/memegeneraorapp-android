package com.example.memegenerator.Models;

public class RecyclerModel {
    private String image = "";
    private String name = "";

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage_fav() {
        return image_fav;
    }

    public void setImage_fav(String image_fav) {
        this.image_fav = image_fav;
    }

    private String image_fav = "";
}
