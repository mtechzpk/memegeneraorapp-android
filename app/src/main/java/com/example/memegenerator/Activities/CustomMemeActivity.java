package com.example.memegenerator.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.memegenerator.R;

public class CustomMemeActivity extends AppCompatActivity {
ImageView ivMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_meme);
        ivMenu=findViewById(R.id.ivMenu);
        ivMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPopupButtonClick(view);
            }
        });

    }

    public void onPopupButtonClick(View view) {
        PopupMenu popup = new PopupMenu(this, view);
        popup.getMenuInflater().inflate(R.menu.custom_menu, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
//                Toast.makeText(CustomMemeActivity.this,
//                        "Clicked popup menu item " + item.getTitle(),
//                        Toast.LENGTH_SHORT).show();
                switch(item.getItemId()) {
                    case R.id.Redownload:
                        Toast.makeText(CustomMemeActivity.this, "redownload", Toast.LENGTH_SHORT).show();
                        break;
                        case R.id.Reset:
                        Toast.makeText(CustomMemeActivity.this, "Reset", Toast.LENGTH_SHORT).show();
                        break;
                        case R.id.Rotate:
                        Toast.makeText(CustomMemeActivity.this, "Rotate", Toast.LENGTH_SHORT).show();
                        break;
                        case R.id.Flip:
                        Toast.makeText(CustomMemeActivity.this, "Flip", Toast.LENGTH_SHORT).show();
                        break;
                        case R.id.Zoom:
                        Toast.makeText(CustomMemeActivity.this, "Zoom", Toast.LENGTH_SHORT).show();
                        break;

                }
                return true;
            }
        });

        popup.show();
    }
}
